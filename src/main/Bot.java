import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;

import javax.annotation.Nonnull;
import javax.security.auth.login.LoginException;
import java.util.List;

public class Bot extends ListenerAdapter {

    public static String prefix = "!";

    public void MainBot() throws LoginException, InterruptedException {
        JDA jda = JDABuilder.create("", GatewayIntent.getIntents(GatewayIntent.DEFAULT))
                .setToken("ENTER YOUR TOKEN")
                .addEventListeners(new Bot())
                .build().awaitReady();
    }

    @Override
    public void onMessageReceived(@Nonnull MessageReceivedEvent event) {

        String[] message = event.getMessage().getContentRaw().split("\\s+");

        if (message[0].equalsIgnoreCase(prefix + "clear")) {
            if (message.length < 2) {
                event.getTextChannel().sendMessage("!clear [количество сообщений]").queue();
            } else {
                try {
                    List<Message> messageHistory = event
                            .getTextChannel()
                            .getHistory()
                            .retrievePast(Integer.parseInt(message[1]))
                            .complete();

                    event.getTextChannel().deleteMessages(messageHistory).queue();

                    event.getTextChannel().sendMessage("Удалено " + message[1] + " сообщений/ия").queue();

                } catch (IllegalArgumentException e) {
                    if (e.toString().startsWith("java.lang.IllegalArgumentException: Message retrieval")) {
                        event.getTextChannel().sendMessage("Может быть удалено только от 2 до 100 сообщений.").queue();
                    }
                }
            }
        }

    }
}
